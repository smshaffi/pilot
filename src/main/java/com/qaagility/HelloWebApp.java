package com.qaagility;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWebApp extends HttpServlet {

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {

            PrintWriter out = resp.getWriter();
            
            resp.setContentType("text/html");

            out.println("<html>");
            out.println("<body bgcolor=\"Aqua\">");

            out.println("<h1>Hello World by Shaffi from DevOps Alliance and ATA</h1>");
            resp.getWriter().write("Java Project for CPDOF Training Revision Session by Shaik Mohammed SHAFFI in Jun-2020");

            out.println("</body>");
            out.println("</html>");   
        }


        public int add(int a, int b) {
            return a + b;
        }

	public int sub(int a, int b) {
            return a - b;
        }

}
